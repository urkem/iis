﻿using IIS.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace IIS.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Discount> Discounts { get; set; }
        public DbSet<RoomPrice> RoomPrices { get; set; }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Furniture> Furniture { get; set; }
        public DbSet<Category> Categorys { get; set; }
        public DbSet<Room> Rooms { get; set; }
    }
}
