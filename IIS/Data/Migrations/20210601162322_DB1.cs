﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IIS.Data.Migrations
{
    public partial class DB1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tbCategory",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tbGuest",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbGuest", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tbRooms",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryId = table.Column<long>(type: "bigint", nullable: true),
                    Capacity = table.Column<long>(type: "bigint", nullable: false),
                    m2Area = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    WiFi = table.Column<bool>(type: "bit", nullable: false),
                    Minibar = table.Column<bool>(type: "bit", nullable: false),
                    AirConditioning = table.Column<bool>(type: "bit", nullable: false),
                    PrivateBathroom = table.Column<bool>(type: "bit", nullable: false),
                    Terrace = table.Column<bool>(type: "bit", nullable: false),
                    Floor = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbRooms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tbRooms_tbCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "tbCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tbDiscounts",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Start = table.Column<DateTime>(type: "datetime2", nullable: false),
                    End = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Percentage = table.Column<bool>(type: "bit", nullable: false),
                    LongDiscount = table.Column<bool>(type: "bit", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    RoomId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbDiscounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tbDiscounts_tbRooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "tbRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tbFurnitures",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Specification = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RoomId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbFurnitures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tbFurnitures_tbRooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "tbRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tbReservations",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Start = table.Column<DateTime>(type: "datetime2", nullable: false),
                    End = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    RoomId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbReservations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tbReservations_tbRooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "tbRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "tbRoomPrices",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Start = table.Column<DateTime>(type: "datetime2", nullable: false),
                    End = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    RoomId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbRoomPrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tbRoomPrices_tbRooms_RoomId",
                        column: x => x.RoomId,
                        principalTable: "tbRooms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GuestReservation",
                columns: table => new
                {
                    GuestsId = table.Column<long>(type: "bigint", nullable: false),
                    ReservationsId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GuestReservation", x => new { x.GuestsId, x.ReservationsId });
                    table.ForeignKey(
                        name: "FK_GuestReservation_tbGuest_GuestsId",
                        column: x => x.GuestsId,
                        principalTable: "tbGuest",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GuestReservation_tbReservations_ReservationsId",
                        column: x => x.ReservationsId,
                        principalTable: "tbReservations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GuestReservation_ReservationsId",
                table: "GuestReservation",
                column: "ReservationsId");

            migrationBuilder.CreateIndex(
                name: "IX_tbDiscounts_RoomId",
                table: "tbDiscounts",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_tbFurnitures_RoomId",
                table: "tbFurnitures",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_tbReservations_RoomId",
                table: "tbReservations",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_tbRoomPrices_RoomId",
                table: "tbRoomPrices",
                column: "RoomId");

            migrationBuilder.CreateIndex(
                name: "IX_tbRooms_CategoryId",
                table: "tbRooms",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GuestReservation");

            migrationBuilder.DropTable(
                name: "tbDiscounts");

            migrationBuilder.DropTable(
                name: "tbFurnitures");

            migrationBuilder.DropTable(
                name: "tbRoomPrices");

            migrationBuilder.DropTable(
                name: "tbGuest");

            migrationBuilder.DropTable(
                name: "tbReservations");

            migrationBuilder.DropTable(
                name: "tbRooms");

            migrationBuilder.DropTable(
                name: "tbCategory");
        }
    }
}
