﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IIS.Data.Migrations
{
    public partial class DB2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tbReservations_tbRooms_RoomId",
                table: "tbReservations");

            migrationBuilder.AlterColumn<long>(
                name: "RoomId",
                table: "tbReservations",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_tbReservations_tbRooms_RoomId",
                table: "tbReservations",
                column: "RoomId",
                principalTable: "tbRooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tbReservations_tbRooms_RoomId",
                table: "tbReservations");

            migrationBuilder.AlterColumn<long>(
                name: "RoomId",
                table: "tbReservations",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddForeignKey(
                name: "FK_tbReservations_tbRooms_RoomId",
                table: "tbReservations",
                column: "RoomId",
                principalTable: "tbRooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
