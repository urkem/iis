﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IIS.Data.Migrations
{
    public partial class DB4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tbDiscounts_tbRooms_RoomId",
                table: "tbDiscounts");

            migrationBuilder.DropForeignKey(
                name: "FK_tbRoomPrices_tbRooms_RoomId",
                table: "tbRoomPrices");

            migrationBuilder.AlterColumn<long>(
                name: "RoomId",
                table: "tbRoomPrices",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "RoomId",
                table: "tbDiscounts",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_tbDiscounts_tbRooms_RoomId",
                table: "tbDiscounts",
                column: "RoomId",
                principalTable: "tbRooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_tbRoomPrices_tbRooms_RoomId",
                table: "tbRoomPrices",
                column: "RoomId",
                principalTable: "tbRooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tbDiscounts_tbRooms_RoomId",
                table: "tbDiscounts");

            migrationBuilder.DropForeignKey(
                name: "FK_tbRoomPrices_tbRooms_RoomId",
                table: "tbRoomPrices");

            migrationBuilder.AlterColumn<long>(
                name: "RoomId",
                table: "tbRoomPrices",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<long>(
                name: "RoomId",
                table: "tbDiscounts",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddForeignKey(
                name: "FK_tbDiscounts_tbRooms_RoomId",
                table: "tbDiscounts",
                column: "RoomId",
                principalTable: "tbRooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_tbRoomPrices_tbRooms_RoomId",
                table: "tbRoomPrices",
                column: "RoomId",
                principalTable: "tbRooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
