﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IIS.Data.Migrations
{
    public partial class DB3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "End",
                table: "tbRoomPrices");

            migrationBuilder.DropColumn(
                name: "End",
                table: "tbDiscounts");

            migrationBuilder.RenameColumn(
                name: "Start",
                table: "tbRoomPrices",
                newName: "ValidFor");

            migrationBuilder.RenameColumn(
                name: "Start",
                table: "tbDiscounts",
                newName: "ValidFor");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ValidFor",
                table: "tbRoomPrices",
                newName: "Start");

            migrationBuilder.RenameColumn(
                name: "ValidFor",
                table: "tbDiscounts",
                newName: "Start");

            migrationBuilder.AddColumn<DateTime>(
                name: "End",
                table: "tbRoomPrices",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "End",
                table: "tbDiscounts",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
