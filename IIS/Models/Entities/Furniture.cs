﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IIS.Models.Entities
{
    [Table("tbFurnitures")]
    public class Furniture
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Specification { get; set; }
        public Room Room { get; set; }
    }
}
