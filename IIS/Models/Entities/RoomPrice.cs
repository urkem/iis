﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IIS.Models.Entities
{
    [Table("tbRoomPrices")]
    public class RoomPrice
    {
        [Key]
        public long Id { get; set; }
        public DateTime ValidFor { get; set; }
        [ForeignKey("Room")]
        public long RoomId { get; set; }
        public decimal Amount { get; set; }
    }
}
