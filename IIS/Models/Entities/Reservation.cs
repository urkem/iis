﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IIS.Models.Entities
{
    [Table("tbReservations")]
    public class Reservation
    {
        [Key]
        public long Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public decimal Price { get; set; }
        [ForeignKey("Room")]
        public long RoomId { get; set; }
        public List<Guest> Guests { get; set; }
    }
}
