﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IIS.Models.Entities
{
    [Table("tbRooms")]
    public class Room
    {
        [Key]
        public long Id { get; set; }
        public Category Category { get; set; }
        public uint Capacity { get; set; }
        public decimal m2Area { get; set; }
        public bool WiFi { get; set; }
        public bool Minibar { get; set; }
        public bool AirConditioning { get; set; }
        public bool PrivateBathroom { get; set; }
        public bool Terrace { get; set; }
        public uint Floor { get; set; }
        public List<Reservation> Reservations { get; set; }
        public List<RoomPrice> Prices { get; set; }
        public List<Discount> Discounts { get; set; }
    }
}
