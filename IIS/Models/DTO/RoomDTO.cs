﻿using IIS.Models.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IIS.Models.DTO
{
    public class RoomDTO
    {
        public RoomDTO()
        {

        }
        public RoomDTO(Room room)
        {
            Id = room.Id;
            Capacity = room.Capacity;

            if (room.Category is not null)
                Category = room.Category.Name;

            m2Area = room.m2Area;
            WiFi = room.WiFi;
            AirConditioning = room.AirConditioning;
            PrivateBathroom = room.PrivateBathroom;
            Terrace = room.Terrace;
            Floor = room.Floor;
        }
        public long Id { get; set; }
        public string Category { get; set; }
        public uint Capacity { get; set; }
        public decimal m2Area { get; set; }
        public bool WiFi { get; set; }
        public bool Minibar { get; set; }
        public bool AirConditioning { get; set; }
        public bool PrivateBathroom { get; set; }
        public bool Terrace { get; set; }
        public uint Floor { get; set; }
        public decimal Price { get; set; }
        public DateTime PriceStart { get; set; }
        public DateTime PriceEnd { get; set; }
        public decimal? Discount { get; set; }
        public DateTime? DiscountStart { get; set; }
        public DateTime? DiscountEnd { get; set; }
        public bool IsPercentage { get; set; }
        public bool IsLongTerm { get; set; }
    }
}
