﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IIS.Models.DTO
{
    public class RoomSuggestionDTO
    {
        public string Action { get; set; } // Price correction, Add discount, Add long reservation discount
        public decimal Amount { get; set; } // 10
        public string Reason { get; set; } // Low occupancy
        public long RoomId { get; set; }
    }
}
