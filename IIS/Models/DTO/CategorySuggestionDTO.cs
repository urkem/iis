﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IIS.Models.Entities;

namespace IIS.Models.DTO
{
    public class CategorySuggestionDTO
    {
        public string Action { get; set; } // Price correction, Add discount, Add long reservation discount
        public decimal Amount { get; set; } // 10
        public string Reason { get; set; } // Low occupancy
        public string CategoryName { get; set; } // King
        public long CategoryId { get; set; } // King
    }
}
