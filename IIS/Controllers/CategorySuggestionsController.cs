﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IIS.Data;
using IIS.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using IIS.Services;
using IIS.Models.DTO;

namespace IIS.Controllers
{
    [Authorize]
    public class CategorySuggestionsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IPriceService _priceService;
        private readonly ISuggestionService _suggestionService;

        public CategorySuggestionsController(ApplicationDbContext context, IPriceService priceService, ISuggestionService suggestionService)
        {
            _context = context;
            _priceService = priceService;
            _suggestionService = suggestionService;
        }

        // GET: GeneralSuggestions
        public IActionResult Index()
        {
            return View(_suggestionService.GetCategorySuggestions(DateTime.Now, DateTime.Now.AddDays(1)));
        }

        [HttpPost]
        public IActionResult Index(DateTime start, DateTime end)
        {
            return View(_suggestionService.GetCategorySuggestions(start, end));
        }


        // GET: GeneralSuggestions/Create
        [HttpGet("~/CategorySuggestions/CreateAction/{cat}/{ac}/{am}")]
        public IActionResult CreateAction(long cat, string ac, string am)
        {
            decimal amount;
            if (decimal.TryParse(am,out amount))
            {
                if (UpdatePrices(ac, cat, amount))
                {
                    return View();
                }
                return NotFound();
            }
            return NotFound();
        
        }

        private bool UpdatePrices(string action, long cat, decimal amount)
        {
            int changeDuration = _suggestionService.GetConfig().CSChangeTime;
            if (action.Equals("Add-Discount"))
            {
                var rooms = _context.Rooms
                    .Include(x => x.Category)
                    .Where(x => x.Category.Id == cat)
                    .Select(x => x.Id)
                    .ToList();
                foreach (var roomId in rooms)
                    _priceService.SetDisccount(roomId, true, false, amount, DateTime.Now.Date, DateTime.Now.AddDays(changeDuration).Date);

                return true;
            }
            else if (action.Equals("Price-increase"))
            {
                var rooms = _context.Rooms
                    .Include(x => x.Category)
                    .Where(x => x.Category.Id == cat)
                    .Select(x => x.Id)
                    .ToList();
                foreach (var roomId in rooms)
                {
                    var price = _priceService.GetPrice(roomId, DateTime.Now.Date).Amount * (amount / 100 + 1);
                    _priceService.SetPrice(roomId, price, DateTime.Now.Date, DateTime.Now.AddDays(changeDuration).Date);
                }

                return true;
            }
            else if (action.Equals("Add-Discount-Long"))
            {
                var rooms = _context.Rooms
                    .Include(x => x.Category)
                    .Where(x => x.Category.Id == cat)
                    .Select(x => x.Id)
                    .ToList();
                foreach (var roomId in rooms)
                    _priceService.SetDisccount(roomId, true, true, amount, DateTime.Now.Date, DateTime.Now.AddDays(changeDuration).Date);

                return true;
            }
            return false;
            
        }
    }
}
