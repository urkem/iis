﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IIS.Data;
using IIS.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using IIS.Services;
using IIS.Models.DTO;

namespace IIS.Controllers
{
    [Authorize]
    public class RoomSuggestionsController : Controller
    {
        private readonly IPriceService _priceService;
        private readonly ISuggestionService _suggestionService;


        public RoomSuggestionsController(IPriceService priceService, ISuggestionService suggestionService)
        {
            _priceService = priceService;
            _suggestionService = suggestionService;
        }

        // GET: RoomSuggestions
        public IActionResult Index()
        {
            return View(_suggestionService.GetRoomSuggestions(DateTime.Now, DateTime.Now.AddDays(1)));
        }

        [HttpPost]
        public IActionResult Index(DateTime start, DateTime end)
        {
            return View(_suggestionService.GetRoomSuggestions(start, end));
        }


        [HttpGet("~/RoomSuggestions/CreateAction/{room}/{ac}/{am}")]
        public IActionResult CreateAction(long room, string ac, string am)
        {
            decimal amount;
            if (decimal.TryParse(am, out amount))
            {
                if (UpdatePrices(ac, room, amount))
                {
                    return View();
                }
                return NotFound();
            }
            return NotFound();

        }

        private bool UpdatePrices(string action, long roomId, decimal amount)
        {
            int changeDuration = _suggestionService.GetConfig().RSChangeTime;
            if (action.Equals("Add-Discount"))
            {
                _priceService.SetDisccount(roomId, true, false, amount, DateTime.Now.Date, DateTime.Now.AddDays(changeDuration).Date);
                return true;
            }
            else if (action.Equals("Price-increase"))
            {
                var price = _priceService.GetPrice(roomId, DateTime.Now.Date).Amount * (amount / 100 + 1);
                _priceService.SetPrice(roomId, price, DateTime.Now.Date, DateTime.Now.AddDays(changeDuration).Date);
                return true;
            }
            else if (action.Equals("Add-Discount-Long"))
            {
                _priceService.SetDisccount(roomId, true, true, amount, DateTime.Now.Date, DateTime.Now.AddDays(changeDuration).Date);
                return true;
            }
            
            return false;
        }
    }
}
