﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IIS.Data;
using IIS.Models.Entities;
using Microsoft.AspNetCore.Authorization;
using IIS.Models.DTO;
using IIS.Services;

namespace IIS.Controllers
{
    [Authorize]
    public class RoomsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IUtilsService _utilsService;
        private readonly IPriceService _priceService;

        public RoomsController(ApplicationDbContext context, IUtilsService utilsService, IPriceService priceService)
        {
            _context = context;
            _utilsService = utilsService;
            _priceService = priceService;
        }

        // GET: Rooms
        public async Task<IActionResult> Index()
        {
            var rooms = await _context.Rooms
                .Include(m => m.Category)
                .ToListAsync();

            List<RoomDTO> ret_rooms = new List<RoomDTO>();
            foreach (var room in rooms)
                ret_rooms.Add(SetPrice(new RoomDTO(room)));

            return View(ret_rooms);
        }

        public IActionResult GenerateReport()
        {
            _utilsService.GenerateReport();
            return View();
        }

        // GET: Rooms/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var room = await _context.Rooms
                .Include(m => m.Category)
                .Include(m => m.Prices)
                .Include(m => m.Discounts)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (room == null)
            {
                return NotFound();
            }

            return View(SetPrice(new RoomDTO(room)));
        }

        // GET: Rooms/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Rooms/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Capacity,m2Area,Category,WiFi,Minibar,AirConditioning,PrivateBathroom,Terrace,Floor,Price,PriceStart,PriceEnd,Discount,DiscountStart,DiscountEnd,IsPercentage,IsLongTerm")] RoomDTO room)
        {
            if (ModelState.IsValid)
            {
                var added_room = MakeRoom(room);
                _context.Add(added_room);
                await _context.SaveChangesAsync();

                _priceService.SetPrice(added_room.Id, room.Price, room.PriceStart.Date, room.PriceEnd.Date);

                if (room.Discount is not null)
                {
                    DateTime start = (DateTime)room.DiscountStart;
                    DateTime end = (DateTime)room.DiscountEnd;
                    _priceService.SetDisccount(room.Id, room.IsPercentage, room.IsLongTerm, (decimal)room.Discount, start.Date, end.Date);
                }
                
                return RedirectToAction(nameof(Index));
            }
            return View(room);
        }

        // GET: Rooms/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var room = await _context.Rooms
                .Include(m => m.Category)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (room == null)
            {
                return NotFound();
            }
            return View(SetPrice(new RoomDTO(room)));
        }

        // POST: Rooms/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,Capacity,m2Area,Category,WiFi,Minibar,AirConditioning,PrivateBathroom,Terrace,Floor,Price,PriceStart,PriceEnd,Discount,DiscountStart,DiscountEnd,IsPercentage,IsLongTerm")] RoomDTO room)
        {
            if (id != room.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(UpdateRoom(room));
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RoomExists(room.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(room);
        }

        // GET: Rooms/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var room = await _context.Rooms
                .FirstOrDefaultAsync(m => m.Id == id);
            if (room == null)
            {
                return NotFound();
            }

            return View(room);
        }

        // POST: Rooms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var room = await _context.Rooms.FindAsync(id);
            _context.Rooms.Remove(room);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RoomExists(long id)
        {
            return _context.Rooms.Any(e => e.Id == id);
        }

        private Room UpdateRoom(RoomDTO room)
        {
            
            var ret_room = _context.Rooms.FirstOrDefault(x => x.Id == room.Id);
            if (ret_room is null)
            {
                return null;
            }

            _priceService.SetPrice(room.Id, room.Price, room.PriceStart.Date, room.PriceEnd.Date);

            if (room.Discount is not null)
            {
                DateTime start = (DateTime)room.DiscountStart;
                DateTime end = (DateTime)room.DiscountEnd;
                _priceService.SetDisccount(room.Id, room.IsPercentage, room.IsLongTerm, (decimal)room.Discount, start.Date, end.Date);
            }


            ret_room.Id = room.Id;
            ret_room.Floor = room.Floor;
            ret_room.m2Area = room.m2Area;
            ret_room.Capacity = room.Capacity;
            ret_room.Minibar = room.Minibar;
            ret_room.PrivateBathroom = room.PrivateBathroom;
            ret_room.Terrace = room.Terrace;
            ret_room.WiFi = room.WiFi;
            ret_room.AirConditioning = room.AirConditioning;
            ret_room.Category = _context.Categorys.FirstOrDefault(x => x.Name == room.Category);
            return ret_room;
        }
        private Room MakeRoom(RoomDTO room)
        {
            return new Room
            {
                Floor = room.Floor,
                m2Area = room.m2Area,
                Capacity = room.Capacity,
                Minibar = room.Minibar,
                PrivateBathroom = room.PrivateBathroom,
                Terrace = room.Terrace,
                WiFi = room.WiFi,
                AirConditioning = room.AirConditioning,
                Category = _context.Categorys.FirstOrDefault(x => x.Name == room.Category)
            };
        }
        private RoomDTO SetPrice(RoomDTO room)
        {
            var disc = _priceService.GetDiscount(room.Id, DateTime.Now.Date);
            if (disc is not null){
                room.Discount = disc.Amount;
                room.IsLongTerm = disc.LongDiscount;
                room.IsPercentage = disc.Percentage;
            }

            var price = _priceService.GetPrice(room.Id, DateTime.Now.Date);
            if (price is not null)
            {
                room.Price = price.Amount;
            }

            return room;
        }
    }
}

/*
            if (ret_room is null)
            {
                ret_room = new Room
                {
                    Prices = new List<RoomPrice>(),
                    Discounts = new List<Discount>()
                };
            }

            var pom = ret_room.Prices.Count;
            if (pom == 0)
            {
                ret_room.Prices.Add(new RoomPrice{
                    Amount = room.Price,
                    Start = DateTime.Now
                });
            }
            else if (ret_room.Prices[pom-1].Amount != room.Price)
            {
                ret_room.Prices[pom - 1].End = DateTime.Now;
                ret_room.Prices.Add(new RoomPrice{
                    Amount = room.Price,
                    Start = DateTime.Now
                });
            }

            pom = ret_room.Discounts.Count;
            if (room.Discount is not null)
            {
                if (pom == 0)
                {
                    ret_room.Discounts.Add(new Discount
                    {
                        Amount = (decimal)room.Discount,
                        Start = DateTime.Now,
                        Percentage = room.IsPercentage,
                        LongDiscount = room.IsLongTerm
                    });
                }
                else if (ret_room.Discounts[pom - 1].Amount != room.Discount | ret_room.Discounts[pom - 1].Percentage != room.IsPercentage | ret_room.Discounts[pom - 1].LongDiscount != room.IsLongTerm)
                {
                    ret_room.Discounts[pom - 1].End = DateTime.Now;
                    ret_room.Discounts.Add(new Discount
                    {
                        Amount = (decimal)room.Discount,
                        Start = DateTime.Now,
                        Percentage = room.IsPercentage,
                        LongDiscount = room.IsLongTerm
                    });
                }
            }


            ret_room.Id = room.Id;
            ret_room.Floor = room.Floor;
            ret_room.m2Area = room.m2Area;
            ret_room.Capacity = room.Capacity;
            ret_room.Minibar = room.Minibar;
            ret_room.PrivateBathroom = room.PrivateBathroom;
            ret_room.Terrace = room.Terrace;
            ret_room.WiFi = room.WiFi;
            ret_room.AirConditioning = room.AirConditioning;
            ret_room.Category = _context.Categorys.FirstOrDefault(x => x.Name == room.Category);
            */