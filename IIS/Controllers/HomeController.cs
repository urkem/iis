﻿using IIS.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using IIS.Services;

namespace IIS.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUtilsService _utilsService;

        public HomeController(ILogger<HomeController> logger, IUtilsService utilsService)
        {
            _logger = logger;
            _utilsService = utilsService;
        }

        public IActionResult Index()
        {
            ViewData["occupancy"] = Math.Round(_utilsService.GetOccupancy(DateTime.Now, DateTime.Now.AddDays(1)) * 100,2);
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
