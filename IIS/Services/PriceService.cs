﻿using IIS.Data;
using IIS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using IIS.Models.DTO;
using Newtonsoft.Json;

namespace IIS.Services
{
    public class PriceService : IPriceService
    {
        private readonly ApplicationDbContext _context;
        public PriceService(ApplicationDbContext context)
        {
            _context = context;
        }

        public Discount GetDiscount(long roomId, DateTime date)
        {
            date = date.Date;
            return _context.Discounts.FirstOrDefault(x => x.RoomId == roomId & x.ValidFor == date);
        }

        public RoomPrice GetPrice(long roomId, DateTime date)
        {
            date = date.Date;
            return _context.RoomPrices.FirstOrDefault(x => x.RoomId == roomId & x.ValidFor == date);
        }

        public void SetDisccount(long roomId, bool percentage, bool longdiscount, decimal amount, DateTime start, DateTime end)
        {
            var room = _context.Rooms.Include(x => x.Discounts).FirstOrDefault(x => x.Id == roomId);
            room.Discounts.RemoveAll(x => x.ValidFor >= start & x.ValidFor <= end);

            var iter_date = start;
            while (iter_date <= end)
            {
                room.Discounts.Add(new Discount
                {
                    Amount = amount,
                    RoomId = room.Id,
                    ValidFor = iter_date,
                    LongDiscount = longdiscount,
                    Percentage = percentage
                });
                iter_date = iter_date.AddDays(1);
            }
            _context.Rooms.Update(room);
            _context.SaveChanges();
        }

        public void SetPrice(long roomId, decimal amount, DateTime start, DateTime end)
        {
            var room = _context.Rooms.Include(x => x.Prices).FirstOrDefault(x => x.Id == roomId);
            room.Prices.RemoveAll(x => x.ValidFor >= start & x.ValidFor <= end);
            
            var iter_date = start;
            while (iter_date <= end)
            {
                room.Prices.Add(new RoomPrice
                {
                    Amount = amount,
                    RoomId = room.Id,
                    ValidFor = iter_date
                });
                iter_date = iter_date.AddDays(1);
            }
            _context.Rooms.Update(room);
            _context.SaveChanges();
        }

    }
}
