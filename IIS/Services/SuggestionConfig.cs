﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IIS.Services
{
    public class SuggestionConfig
    {
        public double GSNormalLevel { get; set; }
        public double GSHighLevel { get; set; }
        public double GSLowLevel { get; set; }
        public double GSLongLevel { get; set; }
        public double CSNormalLevel { get; set; }
        public double CSHighLevel { get; set; }
        public double CSLowLevel { get; set; }
        public double CSLongLevel { get; set; }
        public double RSNormalLevel { get; set; }
        public double RSHighLevel { get; set; }
        public double RSLowLevel { get; set; }
        public double RSLongLevel { get; set; }
        public int GSChangeTime{ get; set; }
        public int CSChangeTime { get; set; }
        public int RSChangeTime { get; set; }
        public DateTime SeasonStart { get; set; }
        public DateTime SeasonEnd { get; set; }
        public double InSeasonMult { get; set; }
        public double OutSeasonMult { get; set; }

        public SuggestionConfig(double gSNormalLevel, double gSHighLevel, double gSLowLevel, double gSLongLevel, double cSNormalLevel, double cSHighLevel, double cSLowLevel, double cSLongLevel, double rSNormalLevel, double rSHighLevel, double rSLowLevel, double rSLongLevel, int gSChangeTime, int cSChangeTime, int rSChangeTime, DateTime seasonStart, DateTime seasonEnd, double inSeasonMult, double outSeasonMult)
        {
            GSNormalLevel = gSNormalLevel;
            GSHighLevel = gSHighLevel;
            GSLowLevel = gSLowLevel;
            GSLongLevel = gSLongLevel;
            CSNormalLevel = cSNormalLevel;
            CSHighLevel = cSHighLevel;
            CSLowLevel = cSLowLevel;
            CSLongLevel = cSLongLevel;
            RSNormalLevel = rSNormalLevel;
            RSHighLevel = rSHighLevel;
            RSLowLevel = rSLowLevel;
            RSLongLevel = rSLongLevel;
            GSChangeTime = gSChangeTime;
            CSChangeTime = cSChangeTime;
            RSChangeTime = rSChangeTime;
            SeasonStart = seasonStart;
            SeasonEnd = seasonEnd;
            InSeasonMult = inSeasonMult;
            OutSeasonMult = outSeasonMult;
        }
    }
}
