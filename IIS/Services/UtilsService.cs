﻿using IIS.Data;
using IIS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using IIS.Models.DTO;

namespace IIS.Services
{
    public class UtilsService : IUtilsService
    {
        private readonly ApplicationDbContext _context;
        public UtilsService(ApplicationDbContext context)
        {
            _context = context;
        }

        public void GenerateReport()
        {
            string _path = "../Report_rooms.pdf";

            Document doc = new Document(PageSize.A4, 10, 10, 40, 35);
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(_path, FileMode.Create));
            Font font = FontFactory.GetFont(FontFactory.HELVETICA, 9);

            List<RoomDTO> rooms = new List<RoomDTO>();
            var rooms_data = _context.Rooms
                .Include(m => m.Category)
                .ToList();

            var date = DateTime.Now.Date;
            foreach (var room in rooms_data)
            {
                var room_insert = new RoomDTO(room);
                var price = _context.RoomPrices.FirstOrDefault(x => x.RoomId == room.Id & x.ValidFor == date);
                if (price is not null)
                    room_insert.Price = price.Amount;
                rooms.Add(room_insert);

            }

            doc.Open();
            doc.AddTitle("Hotel rooms report");
            Paragraph par = new Paragraph("All the rooms in the hotel and prices as of " + DateTime.Now.ToString());
            par.Alignment = Element.ALIGN_CENTER;
            par.SetLeading(5, 5);
            par.SpacingAfter = 20;
            doc.Add(par);

            PdfPTable table = new PdfPTable(11);
            table.WidthPercentage = 100;
            
            table.AddCell(new Phrase("Room Number", font));
            table.AddCell(new Phrase("Category", font));
            table.AddCell(new Phrase("Capacity", font));
            table.AddCell(new Phrase("Area (m2)", font));
            table.AddCell(new Phrase("WiFi", font));
            table.AddCell(new Phrase("Minibar", font));
            table.AddCell(new Phrase("Air conditioner", font));
            table.AddCell(new Phrase("Private bathroom", font));
            table.AddCell(new Phrase("Terrace", font));
            table.AddCell(new Phrase("Floor", font));
            table.AddCell(new Phrase("Price", font));

            foreach (var room in rooms)
            {
                table.AddCell(new Phrase(room.Id.ToString()));
                table.AddCell(new Phrase(room.Category));
                table.AddCell(new Phrase(room.Capacity.ToString()));
                table.AddCell(new Phrase(room.m2Area.ToString()));
                table.AddCell(new Phrase(room.WiFi.ToString()));
                table.AddCell(new Phrase(room.Minibar.ToString()));
                table.AddCell(new Phrase(room.AirConditioning.ToString()));
                table.AddCell(new Phrase(room.PrivateBathroom.ToString()));
                table.AddCell(new Phrase(room.Terrace.ToString()));
                table.AddCell(new Phrase(room.Floor.ToString()));
                table.AddCell(new Phrase(room.Price.ToString()));
            }

            doc.Add(table);
            doc.Close();
        }

        public double GetOccupancy(DateTime start, DateTime end)
        {
            List<Room> rooms = _context.Rooms.Include(m => m.Reservations).ToList();
            List<double> occupancy = new List<double>();
            double totalDays = Math.Round((end - start).TotalDays, 2);
            foreach (Room room in rooms)
            {
                var reservations = room.Reservations.Where(m => m.End >= start).ToList();

                var total_occupied = 0;
                DateTime temp_date = start.AddMinutes(10);
                while (temp_date < end)
                {
                    if (reservations.Exists(m => m.Start <= temp_date & temp_date <= m.End))
                    {
                        total_occupied += 1;
                    }
                    temp_date = temp_date.AddDays(1);
                }
                occupancy.Add(total_occupied / totalDays);
            }
            return occupancy.Average();
        }

        public double GetOccupancyForCategory(Category categorie, DateTime start, DateTime end)
        {
            List<Room> rooms = _context.Rooms
                .Include(m => m.Reservations)
                .Include(m => m.Category)
                .Where(m => m.Category.Id == categorie.Id)
                .ToList();
            List<double> occupancy = new List<double>();
            double totalDays = Math.Round((end - start).TotalDays, 2);
            foreach (Room room in rooms)
            {
                var reservations = room.Reservations.Where(m => m.End >= start).ToList();

                var total_occupied = 0;
                DateTime temp_date = start;
                while (temp_date <= end)
                {
                    if (reservations.Exists(m => m.Start <= temp_date & temp_date <= m.End))
                    {
                        total_occupied += 1;
                    }
                    temp_date = temp_date.AddDays(1);
                }
                occupancy.Add(total_occupied / totalDays);
            }
            if (occupancy.Count > 0)
                return occupancy.Average();
            else
                return 0;
        }

        public double GetOccupancyForRoom(Room room, DateTime start, DateTime end)
        {
            double totalDays = Math.Round((end - start).TotalDays, 2);
            var room1 = _context.Rooms.Include(m => m.Reservations).FirstOrDefault(m => m.Id == room.Id);
            var reservations = room1.Reservations.Where(m => m.End >= start).ToList();

            var total_occupied = 0;
            DateTime temp_date = start;
            while (temp_date <= end)
            {
                if (reservations.Exists(m => m.Start <= temp_date & temp_date <= m.End))
                {
                    total_occupied += 1;
                }
                temp_date = temp_date.AddDays(1);
            }
            return total_occupied / totalDays;
        }
    }
}
