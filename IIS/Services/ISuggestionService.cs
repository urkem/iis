﻿using IIS.Models.Entities;
using IIS.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IIS.Services
{
    public interface ISuggestionService
    {
        public List<RoomSuggestionDTO> GetRoomSuggestions(DateTime start, DateTime end);
        public List<CategorySuggestionDTO> GetCategorySuggestions(DateTime start, DateTime end);
        public List<GeneralSuggestionDTO> GetGeneralSuggestions(DateTime start, DateTime end);
        public SuggestionConfig GetConfig();
    }
}
