﻿using IIS.Data;
using IIS.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using IIS.Models.DTO;
using Newtonsoft.Json;

namespace IIS.Services
{
    public class SuggestionService : ISuggestionService
    {
        private readonly ApplicationDbContext _context;
        private readonly IUtilsService _utilsService;
        private readonly SuggestionConfig _suggestionConfig;

        public SuggestionService(ApplicationDbContext context, IUtilsService utilsService)
        {
            _context = context;
            _utilsService = utilsService;
            using (StreamReader r = new StreamReader("../suggestionConfig.json"))
            {
                string json = r.ReadToEnd();
                _suggestionConfig = JsonConvert.DeserializeObject<SuggestionConfig>(json);
            }
        }

        public List<CategorySuggestionDTO> GetCategorySuggestions(DateTime start, DateTime end)
        {
            double occupancy = _utilsService.GetOccupancy(start, end);
            List<Category> categories = _context.Categorys.ToList();
            List<CategorySuggestionDTO> ret_list = new List<CategorySuggestionDTO>();

            var seasonMult = SeasonMult(start, end);

            foreach (var category in categories)
            {
                double occupancyCategory = _utilsService.GetOccupancyForCategory(category, start, end);

                if (occupancyCategory <= occupancy * _suggestionConfig.CSNormalLevel * seasonMult)
                    ret_list.Add(new CategorySuggestionDTO
                    {
                        Action = "Add-Discount",
                        Amount = 10,
                        Reason = "Low category occupancy",
                        CategoryId = category.Id,
                        CategoryName = category.Name
                    });

                if (occupancyCategory <= occupancy * _suggestionConfig.CSLowLevel * seasonMult)
                    ret_list.Add(new CategorySuggestionDTO
                    {
                        Action = "Add-Discount",
                        Amount = 20,
                        Reason = "Very low category occupancy",
                        CategoryId = category.Id,
                        CategoryName = category.Name
                    });

                if (occupancyCategory <= occupancy * _suggestionConfig.CSLongLevel * seasonMult)
                    ret_list.Add(new CategorySuggestionDTO
                    {
                        Action = "Add-Discount-Long",
                        Amount = 10,
                        Reason = "Very low category occupancy",
                        CategoryId = category.Id,
                        CategoryName = category.Name
                    });
                

                if (occupancyCategory > occupancy * _suggestionConfig.CSHighLevel * seasonMult)
                    ret_list.Add(new CategorySuggestionDTO
                    {
                        Action = "Price-increase",
                        Amount = 10,
                        Reason = "High category occupancy",
                        CategoryId = category.Id,
                        CategoryName = category.Name
                    });
            }

            return ret_list;
        }

        public List<GeneralSuggestionDTO> GetGeneralSuggestions(DateTime start, DateTime end)
        {
            double occupancy = _utilsService.GetOccupancy(start, end);
            List<GeneralSuggestionDTO> ret_list = new List<GeneralSuggestionDTO>();

            var seasonMult = SeasonMult(start, end);

            if (occupancy <= _suggestionConfig.GSNormalLevel * seasonMult)
                ret_list.Add(new GeneralSuggestionDTO
                {
                    Action = "Add-Discount",
                    Amount = 10,
                    Reason = "Low hotel occupancy"
                });

            if (occupancy <= _suggestionConfig.GSLowLevel * seasonMult)
                ret_list.Add(new GeneralSuggestionDTO
                {
                    Action = "Add-Discount",
                    Amount = 20,
                    Reason = "Very low hotel occupancy"
                });

            if (occupancy <= _suggestionConfig.GSLongLevel * seasonMult)
                ret_list.Add(new GeneralSuggestionDTO
                {
                    Action = "Add-Discount-Long",
                    Amount = 20,
                    Reason = "Very low hotel occupancy"
                });

            if (occupancy >= _suggestionConfig.GSHighLevel * seasonMult)
                ret_list.Add(new GeneralSuggestionDTO
                {
                    Action = "Price-increase",
                    Amount = 10,
                    Reason = "High hotel occupancy"
                });

            return ret_list;
        }

        public List<RoomSuggestionDTO> GetRoomSuggestions(DateTime start, DateTime end)
        {
            double occupancy = _utilsService.GetOccupancy(start, end);

            List<Room> rooms = _context.Rooms.ToList();
            List<RoomSuggestionDTO> ret_list = new List<RoomSuggestionDTO>();

            var seasonMult = SeasonMult(start, end);

            foreach (var room in rooms)
            {
                double occupancyRoom = _utilsService.GetOccupancyForRoom(room, start, end);

                if (occupancyRoom <= occupancy * _suggestionConfig.RSNormalLevel * seasonMult)
                    ret_list.Add(new RoomSuggestionDTO
                    {
                        Action = "Add-Discount",
                        Amount = 10,
                        Reason = "Low room occupancy",
                        RoomId = room.Id
                    });

                if (occupancyRoom <= occupancy * _suggestionConfig.RSLowLevel * seasonMult)
                    ret_list.Add(new RoomSuggestionDTO
                    {
                        Action = "Add-Discount",
                        Amount = 20,
                        Reason = "Very low room occupancy",
                        RoomId = room.Id
                    });

                if (occupancyRoom <= occupancy * _suggestionConfig.RSLongLevel * seasonMult)
                    ret_list.Add(new RoomSuggestionDTO
                    {
                        Action = "Add-Discount-Long",
                        Amount = 10,
                        Reason = "Very low room occupancy",
                        RoomId = room.Id
                    });

                if (occupancyRoom > occupancy * _suggestionConfig.RSHighLevel * seasonMult)
                    ret_list.Add(new RoomSuggestionDTO
                    {
                        Action = "Price-increase",
                        Amount = 10,
                        Reason = "High room occupancy",
                        RoomId = room.Id
                    });
            }

            return ret_list;
        }

        public SuggestionConfig GetConfig()
        {
            return _suggestionConfig;
        }

        private double SeasonMult(DateTime start, DateTime end)
        {
            if ((_suggestionConfig.SeasonStart <= start) & (start <= _suggestionConfig.SeasonEnd) & (_suggestionConfig.SeasonStart <= end) & (end <= _suggestionConfig.SeasonEnd))
                return _suggestionConfig.InSeasonMult;
            else if ((_suggestionConfig.SeasonStart <= start) & (start <= _suggestionConfig.SeasonEnd) | (_suggestionConfig.SeasonStart <= end) & (end <= _suggestionConfig.SeasonEnd))
                return 1;
            else
                return _suggestionConfig.OutSeasonMult;
        }
    }
}
