﻿using IIS.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IIS.Services
{
    public interface IPriceService
    {
        public RoomPrice GetPrice(long roomId, DateTime date);
        public void SetPrice(long roomId, decimal amount, DateTime start, DateTime end);
        public Discount GetDiscount(long roomId, DateTime date);
        public void SetDisccount(long roomId, bool percentage, bool longdiscount, decimal amount, DateTime start, DateTime end);
    }
}
