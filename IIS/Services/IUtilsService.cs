﻿using IIS.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IIS.Services
{
    public interface IUtilsService
    {
        public double GetOccupancy(DateTime start, DateTime end);
        public double GetOccupancyForCategory(Category categorie, DateTime start, DateTime end);
        public double GetOccupancyForRoom(Room room, DateTime start, DateTime end);
        public void GenerateReport();
    }
}
